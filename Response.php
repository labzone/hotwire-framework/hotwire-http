<?php

namespace HotWire\Http;

/**
 * used for outputing simple text or used in views
 */
class Response
{
    private $content;

    /**
     * create a new response with content and status code
     * @param string  $content    content
     * @param integer $statusCode http status code
     */
    public function __construct($content = null, $statusCode=200)
    {
        $this->content=$content;
        $this->setStatusCode($statusCode);
    }

    /**
     * set http response status code
     * @param integer $code
     */
    public function setStatusCode($code)
    {
        http_response_code($code);

        return $this;
    }

    /**
     * get content
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * set content
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content=$content;

        return $this;
    }

    /**
     * output response
     */
    public function send()
    {
        ob_start();
        echo $this->content;
        ob_end_flush();
    }
}
