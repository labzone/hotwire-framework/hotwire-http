<?php

namespace HotWire\Http;

/**
 * extends Response to output object in JSON
 */
class JsonResponse extends Response
{
    /**
     * output response as JSON
     * @return string json-formatted string
     */
    public function send()
    {
        ob_start();
        echo json_encode($this->content);
        ob_end_flush();
    }
}
