<?php

namespace HotWire\Http;

use HotWire\Util\Collection\ArrayList;

/**
 * encapsulates superglobals variables ($_GET, $_POST, $_COOKIE, $_SERVER and $_FILES)
 */
class Request
{

    private $attributes;

    public function __construct()
    {
        $this->attributes=new ArrayList();
    }

    /**
     * Access to $_SERVER superglobals
     * @return ArrayList
     */
    public function server()
    {
        return new ArrayList($_SERVER);
    }

    /**
     * Access to $_GET superglobals
     * @return ArrayList
     */
    public function query()
    {
        return new ArrayList($_GET);
    }

    /**
     * Access to $_POST superglobals
     * @return ArrayList
     */
    public function request()
    {
        return new ArrayList($_POST);
    }

    /**
     * Access to $_COOKIE superglobals
     * @return ArrayList
     */
    public function cookies()
    {
        return new ArrayList($_COOKIE);
    }

    /**
     * Access to $_FILES superglobals
     * @return ArrayList
     */
    public function files()
    {
        return new ArrayList($_FILES);
    }

    /**
     * get value from $_GET superglobals
     * @param  string $key
     * @param  string $default default value of key not found
     * @return string the value
     */
    public function get($key, $default=null)
    {
        if (isset($_GET[$key])) {
            return htmlspecialchars($_GET[$key], ENT_QUOTES, 'UTF-8');
        }

        return $default;
    }

    /**
     * get path info
     * @return string
     */
    public static function getPathInfo()
    {
        if (isset($_SERVER['PATH_INFO'])) {
            return $_SERVER['PATH_INFO'];
        }

        return null;
    }

    /**
     * set additional attributes to request
     * @param array $attributes
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes=$attributes;

        return $this;
    }

    /**
     * get attributes
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * check if request is GET method
     * @return boolean
     */
    public function isGet()
    {
        return $this->query()->hasItem();
    }

    /**
     * check if request is POST method
     * @return boolean
     */
    public function isPost()
    {
        return $this->request()->hasItem();
    }
}
